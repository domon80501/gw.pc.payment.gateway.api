﻿using System;
using System.Net;

namespace GW.PC.Payment.Gateway.Context
{
    public class PaymentGatewayResponseContext
    {
        public bool Succeeded { get; set; }
        public string Result { get; set; }
    }
}

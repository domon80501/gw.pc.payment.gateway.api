﻿using GW.PC.Core;
using GW.PC.Payment.Gateway.Context;
using GW.PC.Payment.SDK;
using GW.PC.Web.Core;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace GW.PC.Payment.Gateway.Api.Client
{
    public class PaymentGatewayClient : WebApiClientBase
    {
        protected static HttpClient client = null;

        public PaymentGatewayClient()
        {
            client = client ?? new HttpClient
            {
                BaseAddress = new Uri(Constants.AppSettings.Get(
                    AppSettingNames.PaymentGatewayWebApiAddress))
            };

            RoutePrefix = WebApiControllerRoutePrefixes.PaymentGateway;
        }

        protected override HttpClient GetHttpClient()
        {
            return client;
        }

        public Task<PaymentGatewayResponseContext> Request(PaymentGatewayRequestContext context)
        {
            return PostAsJsonAsyncRawResult<PaymentGatewayRequestContext, PaymentGatewayResponseContext>("request", context);
        }
    }
}

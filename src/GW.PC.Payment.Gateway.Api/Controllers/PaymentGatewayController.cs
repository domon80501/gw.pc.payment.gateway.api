﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json.Linq;
using System.Net.Security;
using GW.PC.Payment.SDK;
using GW.PC.Core;
using GW.PC.Web.Core;
using GW.PC.Payment.Gateway.Context;

namespace GW.PC.Payment.Gateway.Api
{
    [RoutePrefix(WebApiControllerRoutePrefixes.PaymentGateway)]
    public class PaymentGatewayController : ApiController
    {
        [Route("request")]
        public new async Task<PaymentGatewayResponseContext> Request(PaymentGatewayRequestContext model)
        {
            HttpClient client = null;
            HttpResponseMessage response = null;
            var guid = Guid.NewGuid().ToString();

            try
            {
                client = GetClient(model.Data);

                SerilogLogger.Logger.LogInformation($"Request {guid}: {JsonConvert.SerializeObject(model)}");

                if (model.HttpMethod == SDK.HttpMethod.Get)
                {
                    response = await client.GetAsync(model.Url);
                }
                else
                {
                    switch (model.ContentType)
                    {
                        case ContentType.Json:
                            response = await client.PostAsync(model.Url,
                                new StringContent(model.Content.ToString(), Encoding.UTF8, "application/json"));
                            break;
                        case ContentType.FormUrlEncoded:
                            response = await client.PostAsync(model.Url,
                                new StringContent(model.Content.ToString(), Encoding.UTF8, "application/x-www-form-urlencoded"));

                            break;
                        case ContentType.Stream:
                            response = await client.PostAsync(model.Url,
                                new StreamContent(new MemoryStream(Encoding.GetEncoding(model.EncodingName).GetBytes(model.Content.ToString()))));

                            break;
                        case ContentType.FormUrlEncodedKeyValue:
                            response = await client.PostAsync(model.Url,
                                new FormUrlEncodedContent(JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(model.Content.ToString())));

                            break;
                        default:
                            throw new NotSupportedException($"Unsupported content type: {model.ContentType}");
                    }
                }

                await response.EnsureSuccessStatusCodeAsync();
                var resultContent = Encoding.GetEncoding(model.EncodingName).GetString(await response.Content.ReadAsByteArrayAsync());
                SerilogLogger.Logger.LogInformation($"Response {guid}: {resultContent}");

                return new PaymentGatewayResponseContext
                {
                    Succeeded = true,
                    Result = resultContent
                };
            }
            catch (Exception ex)
            {
                var error = $"Error {guid}: {model.Url}. Response code: {response?.StatusCode} Details: {ex.ToString()}";
                SerilogLogger.Logger.LogError(error);

                throw new TaskCanceledException(error);
            }
            finally
            {
                client?.Dispose();

                var data = model.Data;
                if (data != null && data.ContainsKey(Constants.GatewayRequestDataItems.IgnoreServerCertificateValidation))
                {
                    ServicePointManager.ServerCertificateValidationCallback -= ServerCertificateValidation;
                }
            }
        }

        private HttpClient GetClient(IDictionary<string, object> data)
        {
            var client = new HttpClient();

            if (data != null)
            {
                if (data.ContainsKey(Constants.GatewayRequestDataItems.IgnoreServerCertificateValidation))
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback += ServerCertificateValidation;
                }

                if (data.ContainsKey(Constants.GatewayRequestDataItems.CertificateFilePath) &&
                    data.ContainsKey(Constants.GatewayRequestDataItems.CertificatePassword))
                {
                    var cert = new X509Certificate2(
                        data[Constants.GatewayRequestDataItems.CertificateFilePath].ToString(),
                        data[Constants.GatewayRequestDataItems.CertificatePassword].ToString(),
                        X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.MachineKeySet);
                    var handler = new WebRequestHandler();
                    handler.ClientCertificates.Add(cert);

                    client = new HttpClient(handler);
                }

                if (data.TryGetValue("headers", out object headers))
                {
                    foreach (var item in ((JObject)headers).ToObject<IDictionary<string, string>>())
                    {
                        client.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }
            }

            return client;
        }
        private bool ServerCertificateValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        private void NetworkExceptionHandler(HttpStatusCode? httpStatusCode)
        {
            var conditions = new List<HttpStatusCode?>
            {
                 HttpStatusCode.GatewayTimeout,
            };

            if (conditions.Contains(httpStatusCode))
            {
                throw new HttpResponseException((HttpStatusCode)httpStatusCode);
            }
        }
    }
}